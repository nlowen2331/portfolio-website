import React from 'react'
import {Link} from 'react-router-dom'

const Landing=(
    <div>
        I'm a Full Stack Web Developer. I create front-end and back-end solutions with the web's hottest most current
        technologies. 
        <p>• I design and deploy responsive front end interfaces</p>
        <p>• I implement scaleable cloud solutions that grow seamlessly alongside your company</p>
        <p>• I architect database design, and create easily accessable reports to help your company store and analyze data</p>
        <p>• I manage and update your company's pre-existing infrastructure</p>
    </div>
)

const WorkExperience=(
    <div>
        I've been working in the field for just about two years now professionally and through internships.
        <p>• I designed an auction application for Kiwanis International </p>
        <p>• I created cloud architecture from scratch for a New England property management company</p>
        <p>• I've live tested with 50+ app users and recieved live product feedback</p>
        <p>• I've advised small businesses and start-ups on developing and maintaining web presence</p> 
    </div>
)

const Skills = (
    <div>
        I'm familiar with many languages, technologies and frameworks, and I love to learn more and more.
        <p>• Advanced knowledge of Java & Javascript</p>
        <p>• Intermediate knowledge of C#, C++, & Python</p>
        <p>• Advanced in the Javascript framework React (including React Redux, React Router, & react spring)</p>
        <p>• Proficient in SQL and database normalization</p>
        <p>• Proficient in Git and CI/CD in agile environments</p>
        <p>• Proficient with AWS (EC2, S3, SNS, SES, IAM, Lambda)</p>
        <p>• Experiencing managing building and testing local builds of cloud solutions with AWS SAM and Docker</p>
    </div>
)

const TargetPosition = (
    <div>
        <p>
        I'm looking for companies/people/organizations that I can help grow, or I can learn from myself.
        I thrive in fast-paced work environments, and I have a passion for learning and bettering my
        skillset. I love working with people who love what they do. If you believe I could be an 
        asset to you in any way, don't hesitate to reach out!
        </p>
    </div>
)

const AboutData = [
    {title: 'What do I do?',text: Landing},
    {title: 'What have I done?',text: WorkExperience},
    {title: 'What are my skills?', text: Skills},
    {title: 'Where do I want to be?', text: TargetPosition}
]

export default AboutData