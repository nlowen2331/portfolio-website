
const ShowcaseData = [
    {
        title: 'Fully Deployed Web App for K&M Properties',
        srcLink: 'https://gitlab.com/nlowen2331/km_webapp/',
        visLink: 'https://www.sandandskirental.com/',
        list: [
            'Web App for users to browse and book stays in New England rental homes',
            'Created with React',
            'Serves pages from the client-side with react-router',
            'Uses react-spring for animations',
            'Served from a Node Express server running on an EC2',
            'Features a user friendly, highly responsive front-end UI',
            'Allows property managers to manage reservation requests and property listings'
        ]
    },
    {
        title: 'Node Express Backend for K&M Properties',
        srcLink: 'https://gitlab.com/nlowen2331/kandmserver/',
        list: [
            'RESTful API to communicate back-end data to a front-end Web App',
            'Created in Node JS with the Express framework',
            'Runs on an EC2 instance, fully scaleable and cloud-based',
            'Updates local MySQL server and AWS S3 Buckets with user/manager data',
            'Handles notifications from AWS',
            'Authenticates API users',
        ]
    },
    {
        title: 'Check out the Source Code for this site!',
        srcLink: 'https://gitlab.com/nlowen2331/portfolio-website',
        text__deprecated: `
            The source code of the site is below! The site is staticly hosted
            from an S3 Bucket. It utilizes react-router to serve multiple pages
            on the client-side. The styling is pure CSS aside from the use
            of react-spring for some of the transition animations.
        `,
        list: [
            'Created with React and utilizes AWS Lambda functions for all back-end requests',
            'Highly responsive UI',
            'Serves pages from the client-side with react-router',
            'Fully custom CSS and design'
        ]
    },
    {
        title: 'Kiwanis Auction Block Mobile Application',
        text__deprecated: `
            This project is my oldest. It is a React Native application for
            the Kiwanis charity organization. I interned with Kiwanis my 3rd
            year of college. The app allows users to see auction items in real time.
            The user can filter and sort items to their liking. The app sends 
            push notifications when the user has been outbid or has won an item.
            Unfortunately I cannot release the source code. Soon I hope
            to have a visual demo to show. Stay tuned if you are interested.
        `,
        list: [
            'Allows users to browse and see bids on live auction items',
            'React Native mobile application',
            'Created for a paid internship with Kiwanis International',
            'Successfully tested during a live auction, with feedback from users'
        ]
    },
    {
        title: 'Contact Me Lambda Function',
        list: [
            'AWS Lambda function that formats http POST requests to Emails and sends them to desired addresses',
            'Built in Node js',
            'Used on this site\'s contact form',
            'Lightweight and transportable server-less function'
        ],
        srcLink: 'https://gitlab.com/nlowen2331/serverless-contact-me'
    },
    {
        title: 'Phrase Finder Application',
        list: [
            'Allows users to upload a text file and search for phrases within that body of text',
            'Built in React',
            'Uses a Lambda function on the back-end',
            'Publicly accessible'
        ],
        srcLink: 'https://gitlab.com/nlowen2331/phrase-finder-front-end',
        visLink: 'http://phrase-finder-front.s3-website.us-east-2.amazonaws.com/'
    },
]

export default ShowcaseData;