import React, { useEffect } from 'react'
import Navbar from './Components/Navbar'
import Routes from './routes'
import './css/App.css'
import Footer from './Components/Footer'

function App() {

    return (
        <>  
            <Navbar/>
            <Routes/>
            <Footer/>
        </>
    )
}

export default App
