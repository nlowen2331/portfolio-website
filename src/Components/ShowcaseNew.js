import React from 'react'
import ShowcaseData from '../data/ShowcaseData'
import useMedia from '../hooks/useMedia'
import ShowcaseCard from './ShowcaseCard'

function ShowcaseNew() {

    const gridContainerHeight = useMedia(['(max-width: 1150px)','(max-width: 1630px )'],[1,2],3)

    return (
        <div className='showcase-n-main' style={{height: `${Math.ceil(ShowcaseData.length/gridContainerHeight)*500}px`}}>
            <h1>My Projects</h1>
            <div className='showcase-n-grid'>
                {ShowcaseData.map((data,index)=>(
                    <ShowcaseCard {...data} key={index}/>
                ))}
            </div>
        </div>
    )
}

export default ShowcaseNew
