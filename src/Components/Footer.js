import React from 'react'
import '../css/Footer.css'

function Footer() {
    return (
        <div className='footer-main'>
            <div>
                Phone: 978-806-6167
            </div>
            <div>
                Email: nicholasrussellweb@gmail.com
            </div>
        </div>
    )
}

export default Footer
