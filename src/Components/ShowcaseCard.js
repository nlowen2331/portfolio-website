import React from 'react'
import { animated } from 'react-spring'
import {AiOutlineGitlab} from 'react-icons/ai'
import {BsEye} from 'react-icons/bs'

function ShowcaseCard({title,list,visLink,srcLink}) {
    return (
        <div className='showcase-n-card-main'>
            <h2>{title}</h2>
            <div className='showcase-n-p-container'>
                {list.map(item=>(
                    <p>• {item}</p>
                ))}
            </div>
            <div className='showcase-n-btn-container'>
                <LinkButton link={visLink} type={'vis'}/>
                <LinkButton link={srcLink} type={'src'}/>
            </div>
        </div>
    )
}

const LinkButton = ({link,type}) => ( !link||link==='' ? 
    <a className='link-btn-a'><button className='showcase-link-btn' style={{backgroundColor: 'lightgray',cursor: 'default',
        pointerEvents: 'none',border: '1px solid lightgray', color: 'rgb(100,100,100)'}}>
        {type==='vis' ?<BsEye className='showcase-icon'/> : <></>}
        {type==='src' ? <AiOutlineGitlab className='showcase-icon'/> : <></>}
        <div>{type==='vis' ? 'Visualize' : 'Source'}</div>
    </button></a>
    :
    <a className='link-btn-a' href={link}  target="_blank" rel="noopener noreferrer"><button className='showcase-link-btn'>
        {type==='vis' ?<BsEye className='showcase-icon'/> : <></>}
        {type==='src' ? <AiOutlineGitlab className='showcase-icon'/> : <></>}
        <div>{type==='vis' ? 'Visualize' : 'Source'}</div>
    </button></a>
)

export default ShowcaseCard
