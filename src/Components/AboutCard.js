import React from 'react'

function AboutCard({text,title}) {

    return (
        <div className='about-card-main'>
            <h1>
                {title}
            </h1>
            <p>{text}</p>
        </div>
    )
}

export default AboutCard
