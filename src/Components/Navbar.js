import React,{useState} from 'react'
import { Link } from 'react-router-dom'
import '../css/Navbar.css'
import {animateScroll} from 'react-scroll'
import useMedia from '../hooks/useMedia'
import {GiHamburgerMenu} from 'react-icons/gi'
import {MdClose} from 'react-icons/md'
import { animated, useTransition } from 'react-spring'

function Navbar() {

    const defaultMenuLayout = useMedia(['(min-width: 701px )'],[1],0)
    const [sideBarOn,toggleSidebar] = useState(false)

    const transitions = useTransition(sideBarOn,null,{
        unique: true,
        enter: {opacity: 1},
        from: {opacity: 0},
        leave: {opacity: 0,position: 'absolute'},
    })

    return (
        <div>
            <div style={{transform: sideBarOn||defaultMenuLayout===1 ? `translateX(0px)` : `translateX(-300px)`}} className='navbar-main'>
                
                <div className='navbar-title'>
                    <Link to='/' className='navbar-link' onClick={()=>animateScroll.scrollToTop()}>Nicholas Russell Lowe</Link> 
                </div>

                <div className='navbar-link-container'>

                    <Link to='/' id='home-link' className='navbar-link' onClick={()=>toggleSidebar(false)}>Home</Link>

                    <Link to='/About' className='navbar-link' onClick={()=>toggleSidebar(false)}>About</Link>

                    <Link to='/Contact' className='navbar-link' onClick={()=>toggleSidebar(false)}>Contact Me</Link>

                </div>

            </div>
            <div className='nav-sticky-container'>
                <div className='navbar-icon-container' onClick={()=>toggleSidebar(!sideBarOn)}>
                    {transitions.map(({item,key,props})=>(
                        item ?
                        <animated.div style={props}>
                            <MdClose style={{color: 'white'}} className='navbar-icon'/>
                        </animated.div>
                        :
                        <animated.div style={props}>
                            <GiHamburgerMenu className='navbar-icon'/>
                        </animated.div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Navbar
