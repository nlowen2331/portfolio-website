import React,{useState} from 'react'
import '../css/About.css'
import AboutData from '../data/AboutData.js'
import AboutCard from '../Components/AboutCard.js'
import { useSpring,animated,config } from 'react-spring'

function About(){

    const [index,setIndex] = useState(0)

    const spring = useSpring({
        from: {opacity: 0,transform: 'translate3d(0,-60px,0)'},
        opacity: 1, 
        transform: 'translate3d(0,0,0)',
        config: config.slow
    })
    return (
        <div className='page-main about-page-main' style={{height: '100vh',minHeight: '900px',overflow: 'auto'}}>
            <animated.div className='about-container' style={spring}>
                <div className='about-nav'>
                    <h1>About Me</h1>
                    <div className='about-options'>
                        {AboutData.map((data,i)=>(
                            <h3 className={i===index ? 'selected-option' : 'unselected-option'}
                                onClick={()=>setIndex(i)}
                            >{data.title}</h3>
                        ))}
                    </div>
                </div>
                <div className='about-body'>
                    <h1>{AboutData[index].title}</h1>
                    {AboutData[index].text}
                </div>
            </animated.div>
        </div>   
    )
}

export default About
