import React from 'react'

function Error404() {
    return (
        <div className='page-main' style={{display: 'flex',flexDirection: 'column',justifyContent:'center',alignItems:'center'}}>
            <div style={{fontWeight: 300,fontSize: '3em'}}>Oops! Couldn't find that page</div>
        </div>
    )
}

export default Error404
