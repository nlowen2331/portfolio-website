import React, { useEffect } from 'react'
import '../css/Landing.css'
import '../css/ShowcaseNew.css'
import {AiOutlineGitlab,AiOutlineLinkedin} from 'react-icons/ai'
import {MdWork} from 'react-icons/md'
import {IoMdDocument} from 'react-icons/io'
import {Link as ScrollLink, animateScroll} from 'react-scroll'
import ShowcaseNew from '../Components/ShowcaseNew'
import { animated, config, useSpring } from 'react-spring'

function Landing() {

    const spring = useSpring({
        from: {opacity: 0, transform: `translate3d(0,-60px,0)`},
        to: {opacity: 1, transform: `translate3d(0,0,0)`},
        config: config.slow
    })

    useEffect(()=>{
        animateScroll.scrollToTop();
    },[])

    return (
        <div>
            <div className='page-main' style={{display:'flex',flexDirection:'column',justifyContent:'center'}}>

            <div className='landing-content-container'>

                    <animated.h1 style={{fontSize: '4em', fontWeight: '300',
                        padding: '0 20px 0 20px', ...spring}}>
                        Nicholas Russell Lowe
                    </animated.h1>
                    
                    <div className='landing-sub-title'>
                        Web Development, Planning, Maintenence, & Solutions
                    </div>

                    <div className='landing-icons-container'>

                        <div className='landing-icon-inner'>
                            <a href='https://gitlab.com/nlowen2331' target="_blank" rel="noopener noreferrer">
                                <AiOutlineGitlab className='landing-icon' title='Go to my Gitlab Account'/>
                            </a>
                        </div>
                        <div className='landing-icon-inner'>
                            <a href='https://www.linkedin.com/in/nicholas-russell-lowe/' target="_blank" rel="noopener noreferrer">
                                <AiOutlineLinkedin className='landing-icon' title='Go to my Linkedin Account'/>
                            </a>
                        </div>
                        <div className='landing-icon-inner'>
                            <a href='https://public-resume-nrl.s3.us-east-2.amazonaws.com/NRL+Resume.PDF' target="_blank" rel="noopener noreferrer">
                                <IoMdDocument className='landing-icon' title='Open my Resume'/>
                            </a>
                        </div >
                        <div className='landing-icon-inner'>
                            <ScrollLink to='showcase-n-main'
                                smooth={true}
                                offset={-90}
                                duration={900}><MdWork className='landing-icon' 
                                title='Browse my Projects'
                                />
                            </ScrollLink>
                        </div>
                    </div>
                    <div className='landing-get-started-container'>
                        <ScrollLink
                            to='showcase-n-main'
                            smooth={true}
                            duration={900}
                            offset={-90}
                        ><button>My Projects</button></ScrollLink>
                    </div>
                </div>
            </div>
            <ShowcaseNew/>
        </div>
    )
}

export default Landing
