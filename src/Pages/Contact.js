import React, { useState } from 'react'
import { animated, config, useSpring, useTransition } from 'react-spring'
import '../css/Contact.css'

const SITE_KEY = '6LfWYlwaAAAAAIF5RpT_TEQrZh7Q4DZCDBcTR3C-'
const ENDPOINT = 'https://nua5ngft4m.execute-api.us-east-2.amazonaws.com/default/ContactUsSolution'
const MSG_TIMEOUT = 1000*5

function Contact() {

    const [name,changeName] = useState('')
    const [subject,changeSubject] = useState('')
    const [message, changeMessage] = useState('')
    const [responseDisplayMessage, toggleResponseDisplay] = useState([] /* []||{message: '',color: ''} */)

    const spring = useSpring({
        from: {opacity: 0,transform: 'translate3d(0,-60px,0)'},
        opacity: 1, 
        transform: 'translate3d(0,0,0)',
        config: config.slow
    })

    const SendMail= async ()=>{

        let ServerResponse =  await new Promise((resolve,reject)=>{

            window.grecaptcha.ready(async () => {

                const token = await window.grecaptcha.execute(SITE_KEY)

                let Response = await fetch(ENDPOINT,{
                    method: 'POST',
                    headers: {
                        'Content-Type':'application/json',
                    },
                    body: JSON.stringify({name: name,subject:subject,text: message,token: token})
                })

                resolve(Response)

            })

        })

        
        switch(ServerResponse.status) {
            case 200:
                DisplayMessage({
                    msg: 'Successfully sent message! Please allow me time to get back to you.',
                    color: 'lightgreen'
                })
                ResetFields()
                break;
            default:
                DisplayMessage({
                    msg: 'Something went wrong... Please try another form of contact.',
                    color: 'red'
                })
                break;
        }
    }

    const DisplayMessage = (params/*msg,color */)=>{
        toggleResponseDisplay([{...params}])
        setTimeout(()=>toggleResponseDisplay([]),MSG_TIMEOUT)
    }

    const ResetFields = ()=>{
        changeName('')
        changeSubject('')
        changeMessage('')
    }

    const transitions = useTransition(responseDisplayMessage,null,{
        from: {opacity: 0},
        enter: {opacity: 1},
        leave: {opacity: 0}
    })

    return (
        <div className='page-main'>
            <div className='contact-me-outer'>
                <animated.div className="contact-me-inner" style={spring} >

                    <div className='contact-me-title'>
                        <p>Contact Me </p>
                    </div>

                    <div className='contact-me-inputs'>

                        <input placeholder="Name/Organization" 
                            value={name} onChange={(e)=>{changeName(e.target.value)}}
                            maxLength={40}
                        />

                        <input placeholder="Subject"
                            value={subject} onChange={(e)=>{changeSubject(e.target.value)}}
                            maxLength={40}
                        />

                        <textarea placeholder="Message..."
                            value={message} onChange={(e)=>{changeMessage(e.target.value)}}
                            maxLength={400}
                        />

                        <button onClick={()=>SendMail()}>Send</button>

                    </div>

                    <div className='contact-me-res-msg'>
                        {transitions.map(({item,key,props})=>(
                            <animated.div key={key} style={{...props,color: item.color}}>{item.msg}</animated.div>
                        ))}
                    </div>

                </animated.div>
            </div>
        </div>
    )
}

export default Contact
