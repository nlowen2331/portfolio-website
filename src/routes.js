import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Error404 from './Pages/Error404';
import Landing from './Pages/Landing';
import Contact from './Pages/Contact';
import About from './Pages/About';

const routes = (props) => (
  <Switch>
    <Route exact path="/" component={ Landing } />
    <Route exact path="/Contact" component={ Contact} />
    <Route exact path="/About" component={ About } />
    <Route component={ Error404 } />
  </Switch>
);

export default routes