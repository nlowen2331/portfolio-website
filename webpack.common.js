const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
    "entry":["@babel/polyfill", "./src/index.js"],
    "output": {
        "path": __dirname + '/production_build',
        "filename": "bundle.js"
    },
    "plugins": [
        new HtmlWebPackPlugin({
            template: "./public/index.html",
            filename: "./index.html"
        }),
        new MiniCssExtractPlugin()
    ],
    "module": {
        "rules": [
            {
                "enforce": "pre",
                "test": /\.js$/i,
                "exclude": /node_modules/,
                "loader": "eslint-loader",
                "options": {
                  "emitWarning": true,
                  "failOnError": false,
                  "failOnWarning": false
                }
            },
            {
                "test": /\.(js|jsx)$/i,
                "exclude": /node_modules/,
                "use": {
                    "loader": "babel-loader"
                }
            },
            {
                "test": /\.css$/i,
                "use": [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                ]
            },
            {
                "test": /\.html$/i,
                "use": [
                    {
                        "loader": "html-loader"
                    }
                ]
            },
        ]
    },
    "optimization": {
        "minimize": true,
        "minimizer": [
          new CssMinimizerPlugin(),
        ],
      },
}